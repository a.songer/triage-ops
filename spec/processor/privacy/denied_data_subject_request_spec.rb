# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/privacy/denied_data_subject_request'

RSpec.describe Triage::DeniedDataSubjectRequest do
  include_context 'with event', Triage::IssueEvent do
    let(:label_names) { ['deletion request::denied'] }
    let(:event_attrs) do
      {
        label_names: label_names,
        description: issue_description
      }
    end

    let(:issue_description) do
      <<~TEXT
        Form entries

        * Country: United States
        * State: Colorado
      TEXT
    end

    let(:no_previous_comment?) { true }
  end

  subject { described_class.new(event) }

  before do
    allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(no_previous_comment?)
  end

  include_examples 'registers listeners', ['issue.open', 'issue.update']

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#applicable?' do
    let(:has_data_request_project_id?) { true }

    before do
      allow(event).to receive(:with_project_id?).with(Triage::Event::DATA_SUBJECT_REQUEST_PROJECT_ID).and_return(has_data_request_project_id?)
    end

    include_examples 'applicable on contextual event'

    context 'when event is labeled with data-access-request::denied with State: Washington form entry' do
      let(:label_names) { ['data-access-request::denied'] }
      let(:issue_description) do
        <<~TEXT
          Form entries

          * Country: United States
          * State: Washington
        TEXT
      end

      include_examples 'event is not applicable'
    end

    described_class::IMPACTED_STATES.each do |state|
      context "when issue has State:#{state} in form entries" do
        let(:issue_description) do
          <<~TEXT
            Form entries

            * Country: United States
            * State: #{state}
          TEXT
        end

        include_examples 'event is applicable'
      end
    end

    context 'when issue has no request denied labels' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when event is not coming from data subject request project id' do
      let(:has_data_request_project_id?) { false }

      include_examples 'event is not applicable'
    end

    context 'when a denied request issue already has escalated label' do
      let(:label_names) { ['Privacy-Issue-Escalated', 'deletion request::denied'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    context 'when there is a previous comment' do
      let(:no_previous_comment?) { false }

      it 'adds a label' do
        body = <<~MARKDOWN.chomp
          /label ~"Privacy-Issue-Escalated"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when there is no previous comment' do
      it 'adds a label and posts a comment' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          /label ~"Privacy-Issue-Escalated"
          #{described_class::COMMENT_BODY}

          *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
          You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/privacy/denied_data_subject_request.rb).*
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
