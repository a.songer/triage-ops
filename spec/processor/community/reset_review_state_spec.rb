# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/reset_review_state'

RSpec.describe Triage::ResetReviewState do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:added_label_names) { [Labels::WORKFLOW_IN_DEV_LABEL] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update']

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    context 'when "workflow::in dev" is not added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment to remove the ~"automation:reviewers-reminded" label' do
      body = <<~MARKDOWN.chomp
        /unlabel ~"automation:reviewers-reminded"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
