# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'
require_relative '../../triage/processor/react_to_changes'

RSpec.describe Triage::ReactToChanges do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        from_gitlab_org_gitlab?: from_gitlab_org_gitlab,
        merge_event?: merge_event,
        target_branch_is_main_or_master?: target_branch_is_main_or_master,
        object_kind: 'merge_request'
      }
    end

    let(:from_gitlab_org_gitlab) { true }
    let(:merge_event) { true }
    let(:target_branch_is_main_or_master) { true }
    let(:diff) { '+ Enabled: true' }
    let(:comment) { 'review comment' }

    let(:merge_request_changes) do
      {
        'changes' => [
          {
            'old_path' => '.rubocop.yml',
            'new_path' => '.rubocop.yml',
            'diff' => diff
          }
        ]
      }
    end
  end

  include_context 'with merge request discussions' do
    let(:comment_mark) do
      # This is using Triage::NotifyChanges for backward compatibility
      Triage::UniqueComment.new('Triage::NotifyChanges', nil).__send__(:hidden_comment)
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update', 'merge_request.merge']
  it_behaves_like 'processor documentation is present'

  before do # rubocop:disable RSpec/HooksBeforeExamples
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{iid}/changes",
      response_body: merge_request_changes)
  end

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event is not from gitlab-org/gitlab' do
      let(:from_gitlab_org_gitlab) { false }

      include_examples 'event is not applicable'
    end

    context 'when target branch is not main nor master' do
      let(:target_branch_is_main_or_master) { false }

      include_examples 'event is not applicable'
    end

    context 'when there is no changes we are interested' do
      let(:diff) { '' }

      include_examples 'event is not applicable'
    end

    context 'when event is not a merge event' do
      let(:merge_event) { false }

      include_examples 'event is applicable'

      context 'when it is already notified' do
        let(:comment_1) { comment_mark }

        include_examples 'event is not applicable'
      end
    end
  end

  describe '#process' do
    let(:notification) do
      <<~MARKDOWN.strip
        #{comment_mark}
        This merge request is merged with the following changes that might break JiHu:

        - `.rubocop.yml` (NewRuboCop)

        *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
        You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/react_to_changes.rb).*
      MARKDOWN
    end

    context 'when there is already a notification thread' do
      let(:comment_1) { comment_mark }

      before do
        stub_api_request(
          path: "/projects/#{project_id}/merge_requests/#{iid}/discussions",
          query: { per_page: 100 },
          response_body: merge_request_discussions)
      end

      it 'appends a merge notification' do
        stub_api_request(
          verb: :post,
          path: "#{specific_discussion_path}/notes",
          request_body: { body: notification })

        expect { subject.process }.not_to raise_error
      end

      context 'when it is not a merge event' do
        let(:merge_event) { false }

        it 'does not process' do
          expect { subject.process }.not_to raise_error
        end
      end
    end

    context 'when there is no notification thread' do
      before do
        created_discussions = [{
          id: specific_discussion_id, notes: [{ body: comment_mark }]
        }]

        stub_api_request(
          path: "/projects/#{project_id}/merge_requests/#{iid}/discussions",
          query: { per_page: 100 },
          response_body: [])
          .times(1).then
          # Then we created the discussion, so it should be there
          .to_return(body: JSON.dump(created_discussions))
      end

      shared_examples 'post a notification thread' do
        it 'posts a notification thread and resolves it' do
          expect_api_requests do |requests|
            # Post the notification thread
            requests << stub_api_request(
              verb: :post,
              path: discussions_path,
              request_body: { body: notification })

            # And then resolve the thread
            requests << stub_api_request(verb: :put, path: specific_discussion_path, request_body: { 'resolved' => true })

            subject.process
          end
        end
      end

      it_behaves_like 'post a notification thread'

      context 'when it is not a merge event' do
        let(:merge_event) { false }

        let(:notification) do
          <<~MARKDOWN.strip
            #{comment_mark}
            Changes detected for the following files that might break JiHu:

            - `.rubocop.yml` (NewRuboCop)

            *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
            You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/react_to_changes.rb).*
          MARKDOWN
        end

        it_behaves_like 'post a notification thread'
      end
    end
  end
end
