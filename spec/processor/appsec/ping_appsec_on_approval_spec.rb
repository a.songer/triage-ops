# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/appsec/ping_appsec_on_approval'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::PingAppSecOnApproval do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:approver_username) { 'approver' }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org?: true,
        jihu_contributor?: true,
        gitlab_bot_event_actor?: false,
        event_actor_username: approver_username,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  include_context 'with merge request discussions'

  subject { described_class.new(event) }

  include_examples 'registers listeners',
    ['merge_request.approval', 'merge_request.approved']

  describe '#applicable?' do
    context 'when there is no previous comment to ping AppSec' do
      include_examples 'event is applicable'
    end

    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request is not a JiHu contribution' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: "review comment 1" },
          { body: comment_mark, author: { username: Triage::Event::GITLAB_BOT_USERNAME } }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when merge request author is a JiHu contributor' do
      before do
        allow(event).to receive(:jihu_contributor?).and_return(true)
      end

      let(:new_merge_request_discussions) do
        [id: specific_discussion_id, notes: new_merge_request_notes]
      end

      let(:new_merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: thread_mark }
        ]
      end

      let(:body) do
        <<~MARKDOWN.chomp
          #{thread_mark}
          #{comment_mark}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request has been approved. Please wait for AppSec approval before merging.

          cc @gitlab-com/gl-security/appsec this is a ~"JiHu contribution", please follow the [JiHu contribution review process](https://handbook.gitlab.com/handbook/ceo/office-of-the-ceo/jihu-support/jihu-security-review-process/#security-review-workflow-for-jihu-contributions).
        MARKDOWN
      end

      context 'when there is no existing AppSec thread' do
        before do
          stub_api_request(
            path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions",
            query: { per_page: 100 },
            response_body: [])
            .times(1).then
            .to_return(body: JSON.dump(new_merge_request_discussions))
        end

        it 'posts a discussion to request AppSec review' do
          expect_discussion_request(event: event, body: body) do
            subject.process
          end
        end

        context 'when the merge request is already approved by AppSec' do
          let(:label_names) { [described_class::APPSEC_APPROVAL_LABEL] }

          include_examples 'event is not applicable'
        end
      end

      context 'when there is an existing AppSec thread' do
        before do
          stub_api_request(
            path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/discussions",
            query: { per_page: 100 },
            response_body: new_merge_request_discussions)
        end

        it 'posts to existing discussion to request AppSec review' do
          expect_api_requests do |requests|
            # Appends the new note to the approval discussion
            requests << stub_api_request(
              verb: :post,
              path: "#{specific_discussion_path}/notes",
              request_body: { body: body })

            subject.process
          end
        end
      end
    end
  end
end
