# frozen_string_literal: true

require 'spec_helper'

require_relative '../../lib/add_legal_disclaimer_to_epic'

RSpec.describe AddLegalDisclaimerToEpic do
  let(:group_id) { 42 }
  let(:iid) { 12 }
  let(:description_without_legal_disclaimer) { "This is an issue description." }
  let(:description_with_legal_disclaimer) { "#{description_without_legal_disclaimer}\n#{described_class::LEGAL_DISCLAIMER}" }
  let(:description) { description_without_legal_disclaimer }
  let(:resource) do
    {
      group_id: group_id,
      iid: iid,
      description: description_without_legal_disclaimer,
      references: { full: 'gitlab-org&6832' }
    }
  end

  let(:network_class) { Struct.new(:options) }
  let(:epic_class) { Struct.new(:network) }
  let(:api_token) { 'api_token' }
  let(:dry_run) { false }

  subject { described_class.new(epic_object: epic_class.new(network_class.new(double(dry_run: dry_run, token: api_token))), resource: resource) }

  describe '#process' do
    it 'prepends the legal disclaimer to the issue decription' do
      expect_api_request(verb: :put, path: "/groups/#{group_id}/epics/#{iid}", request_body: { description: description_with_legal_disclaimer }) do
        subject.process
      end
    end

    context 'when resource has an old non-versioned disclaimer' do
      let(:description) { description_with_legal_disclaimer.gsub(/ v\d+ /, ' ') }

      it 'replaces the old disclaimer with the new one in the issue decription' do
        expect_api_request(verb: :put, path: "/groups/#{group_id}/epics/#{iid}", request_body: { description: description_with_legal_disclaimer }) do
          subject.process
        end
      end
    end

    context 'when resource has an old versioned disclaimer' do
      let(:description) { description_with_legal_disclaimer.gsub(/ v\d+ /, ' v1 ') }

      it 'replaces the old disclaimer with the new one in the issue decription' do
        expect_api_request(verb: :put, path: "/groups/#{group_id}/epics/#{iid}", request_body: { description: description_with_legal_disclaimer }) do
          subject.process
        end
      end
    end

    context 'when dry-run is true' do
      let(:dry_run) { true }

      it 'prepends the legal disclaimer to the issue decription' do
        expect_no_request(verb: :put, path: "/groups/#{group_id}/epics/#{iid}", request_body: { description: description_with_legal_disclaimer }) do
          expect { subject.process }.to output.to_stdout
        end
      end
    end
  end
end
