# frozen_string_literal: true

require_relative '../../lib/constants/labels'
require_relative '../job/trigger_pipeline_on_approval_job'
require_relative '../triage/event'
require_relative '../triage/need_mr_approved_label'
require_relative '../triage/processor'
require_relative '../triage/unique_comment'

module Triage
  class PipelineTierTransitions < Processor
    TRIGGER_NEW_PIPELINE_DELAY_IN_SECONDS = 5

    PipelineTierTransitionsMessage = Struct.new(:event, :trigger_pipeline_automatically?) do
      def to_s
        <<~MARKDOWN.strip
          ## Before you set this MR to auto-merge

          This merge request will progress on pipeline tiers until it reaches the last tier: ~"#{Labels::PIPELINE_TIER_3_LABEL}".
          #{call_to_action}

          **Before you resolve this discussion**, please check the following:

          * You are **the last maintainer** of this merge request
          * The latest pipeline for this merge request is ~"#{Labels::PIPELINE_TIER_3_LABEL}" (You can find which tier it is in the pipeline name)
          * This pipeline is recent enough (**created in the last 4 hours**)

          If all the criteria above apply, please resolve this discussion and the set auto-merge for this merge request.

          See [pipeline tiers](https://docs.gitlab.com/ee/development/pipelines/#pipeline-tiers) and [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request) for more details.
        MARKDOWN
      end

      def call_to_action
        "We will trigger a new pipeline for each transition to a higher tier." if trigger_pipeline_automatically?
      end
    end

    react_to 'merge_request.open',
      'merge_request.approval',
      'merge_request.approved',
      'merge_request.unapproval',
      'merge_request.unapproved',
      'merge_request.update' # We don't send unapproval/unapproved webhook events when resetting MR approvals from the product.

    def applicable?
      applicable_project? &&
        !event.gitlab_bot_event_actor? &&
        need_pipeline_tier_label?
    end

    def process
      handle_tier_label
      trigger_pipeline_based_on_tier_transition if increasing_tier_transition?
    end

    def documentation
      <<~TEXT
        Ensures that a ~"pipeline::tier-X" label is set (X is a number between 1 and 3) with the following rules:
         - if the MR has no approvals => ~"pipeline::tier-1"
         - if the MR has at at least one approval, but still requires more approvals => ~"pipeline::tier-2"
         - if the MR has all the approvals it needs => ~"pipeline::tier-3"

        Under certain conditions, this processor also will trigger new merge request
        pipelines on increasing transitions between tiers.
      TEXT
    end

    private

    def applicable_project?
      # TODO: Once this processor was tested enough, replace the sandbox by the following
      #
      # event.from_gitlab_org_gitlab? || event.from_gitlab_org_security_gitlab?
      event.from_gitlab_org?(sandbox: true)
    end

    def need_pipeline_tier_label?
      # TODO: When deprecating the mr-approved label, rename this class and its methods to NeedPipelineTierTransitions
      # TODO: Consider further renaming to PipelineTierApplicable and the method to `pipeline_tier_label_applicable?`
      NeedMrApprovedLabel.new(event).need_mr_approved_label?
    end

    def next_tier_label
      @next_tier_label ||=
        if no_approvals?
          Labels::PIPELINE_TIER_1_LABEL
        elsif partially_approved?
          Labels::PIPELINE_TIER_2_LABEL
        elsif fully_approved?
          Labels::PIPELINE_TIER_3_LABEL
        end
    end

    def no_approvals?
      event.approvers.count.zero?
    end

    def partially_approved?
      !no_approvals? && event.approvals.approvals_left.positive?
    end

    def fully_approved?
      event.approvals.approved
    end

    def handle_tier_label
      labels_to_apply = [next_tier_label]
      labels_to_apply << Labels::MR_APPROVED_LABEL if next_tier_above_tier_1?
      apply_labels(labels_to_apply)
    end

    def trigger_pipeline_based_on_tier_transition
      trigger_merge_request_pipeline  if trigger_pipeline_automatically?
      create_pipeline_tier_discussion unless unique_comment.previous_discussion
    end

    def next_tier_above_tier_1?
      !next_tier_integer.nil? && next_tier_integer > 1
    end

    def increasing_tier_transition?
      return false if current_tier_integer.nil? || next_tier_integer.nil?

      current_tier_integer < next_tier_integer
    end

    def current_tier_integer
      @current_tier_integer ||= parse_tier_integer(current_tier_label)
    end

    def next_tier_integer
      @next_tier_integer ||= parse_tier_integer(next_tier_label)
    end

    def parse_tier_integer(label)
      return if label.nil?

      label[/\d+\z/].to_i
    end

    def current_tier_label
      @current_tier_label ||=
        event.label_names.find do |label_name|
          label_name.start_with?(Labels::PIPELINE_TIER_LABEL_PREFIX)
        end
    end

    def trigger_pipeline_automatically?
      event.team_member_author? || event.automation_author?
    end

    def trigger_merge_request_pipeline
      TriggerPipelineOnApprovalJob.perform_in(TRIGGER_NEW_PIPELINE_DELAY_IN_SECONDS, event.noteable_path)
    end

    def create_pipeline_tier_discussion
      discussion = unique_comment.wrap(
        # The .to_s is redundant, but it makes the testing of this method easier.
        PipelineTierTransitionsMessage.new(event, trigger_pipeline_automatically?).to_s
      )

      add_discussion(discussion, append_source_link: false)
    end

    def apply_labels(labels)
      labels_to_apply = labels.select { |label| !event.label_already_present?(label) }
      return if labels_to_apply.empty?

      labels_string = labels_to_apply.map { |label| %(~"#{label}") }.join(' ')
      comment = "/label #{labels_string}"

      add_comment(comment, append_source_link: false)
    end
  end
end
