# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_label'

RSpec.describe Triage::CommandMrLabel do
  before do
    allow(event).to receive(:by_team_member?).and_return(false)
  end

  include_context 'with event', Triage::MergeRequestNoteEvent do
    let(:event_attrs) do
      {
        new_comment: %(#{Triage::GITLAB_BOT} label #{labels})
      }
    end

    let(:labels) { '~"group::import"' }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.note', 'merge_request.note']
  include_examples 'command processor', %w[label unlabel] do
    let(:args_regex) { described_class::LABELS_REGEX }
  end

  describe 'LABELS_REGEX' do
    it { expect(described_class::LABELS_REGEX).to eq(/~"([^"]+)"|~([^ ]+)/) }
  end

  describe 'ALLOWED_LABELS_REGEX' do
    it { expect(described_class::ALLOWED_LABELS_REGEX).to eq(/\A(bug|feature|group|maintenance|type)::[^:]+\z/) }
  end

  describe '#applicable?' do
    include_context 'when command is a valid command from a wider community contribution'

    it_behaves_like 'community contribution command processor #applicable?'

    context 'when event is for a new merge request mentioning the bot' do
      where(:label) do
        [
          'ActivityPub',
          'automation:ml wrong',
          'backend',
          'bug::performance',
          'Category:importers',
          'database',
          'documentation',
          'feature::addition',
          'frontend',
          'group::import',
          'handbook',
          'maintenance::refactor',
          'pipeline:run-all-jest',
          'pipeline:run-all-rspec',
          'security',
          'type::feature',
          'UX',
          'workflow::blocked',
          'workflow::in dev',
          'workflow::ready for review'
        ]
      end

      let(:labels) { %(~"#{label}") }

      with_them do
        it_behaves_like 'event is applicable'
      end
    end

    it_behaves_like 'rate limited'
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'message posting' do |command: 'label', expected_labels: nil, response_body: {}, response_status: 200, expected_error: nil, expected_message: nil|
      it "posts /#{command} command" do
        body = <<~MARKDOWN.chomp
          #{expected_message}

          /#{command} #{expected_labels || labels}
        MARKDOWN

        expect_comment_request(event: event, body: body, response_body: response_body, response_status: response_status) do
          if expected_error
            expect { subject.process }.to raise_error(expected_error)
          else
            expect { subject.process }.not_to raise_error
          end
        end
      end
    end

    context 'when scoped add only label is used' do
      let(:labels) { '~"severity::1"' }

      context 'when label scope does not already exist in issue' do
        before do
          allow(event).to receive(:label_names).and_return(['bug::performance', 'backend'])
        end

        it_behaves_like 'message posting'
      end

      context 'when label scope already exists in issue' do
        before do
          allow(event).to receive(:label_names).and_return(['severity::3', 'bug::performance', 'backend'])
        end

        it_behaves_like 'message posting', expected_message: 'Some labels are not valid: ~"severity::1"', expected_labels: '', command: 'label'
      end

      context 'when command is unlabel' do
        before do
          allow(event).to receive(:new_comment).and_return(%(#{Triage::GITLAB_BOT} unlabel #{labels}))
        end

        it_behaves_like 'message posting', expected_message: 'Some labels are not valid: ~"severity::1"', expected_labels: '', command: 'unlabel'
      end
    end

    context 'when event is for a new issue mentioning the bot' do
      before do
        allow(event).to receive(:note?).and_return(false)
        allow(event).to receive(:new_entity?).and_return(true)
        allow(event).to receive(:issue?).and_return(true)
      end

      context 'when label is ~"group::import"' do
        it_behaves_like 'message posting'
      end

      context 'when command is unlabel' do
        include_context 'when command is a valid command from a wider community contribution'

        before do
          allow(event).to receive(:new_comment).and_return(%(#{Triage::GITLAB_BOT} unlabel #{labels}))
        end

        it_behaves_like 'message posting', command: 'unlabel'

        context 'when label is only allowed to be added' do
          where(:label) do
            [
              'security'
            ]
          end

          let(:labels) { %(~"#{label}") }

          with_them do
            context 'when author is community member' do
              it_behaves_like 'message posting', expected_message: 'Some labels are not valid: ~"security"', expected_labels: '', command: 'unlabel'
            end

            context 'when author is team member' do
              before do
                allow(event).to receive(:by_team_member?).and_return(true)
              end

              it_behaves_like 'message posting', expected_message: 'Some labels are not valid: ~"security"', expected_labels: '', command: 'unlabel'
            end
          end
        end
      end

      context 'when command is not at the beginning of the new_comment' do
        before do
          allow(event).to receive(:new_comment).and_return(%(Hello\n#{Triage::GITLAB_BOT} label #{labels}\nWorld!))
        end

        it_behaves_like 'message posting'

        context 'with multiple labels on the same line with extra spaces' do
          let(:labels) { '  ~"group::import"   ~"group::source code"   ' }

          it_behaves_like 'message posting', expected_labels: '~"group::import" ~"group::source code"'
        end

        context 'with multiple commands on separate lines' do
          before do
            allow(event).to receive(:new_comment).and_return(%(Hello\n#{Triage::GITLAB_BOT} label #{labels}\nWorld!\n#{Triage::GITLAB_BOT} label ~group::foo))
          end

          it_behaves_like 'message posting', expected_labels: '~"group::import"'
        end
      end

      context 'with setting both valid and invalid labels' do
        let(:labels) { '~"group::import" ~"abcd"' }

        it_behaves_like 'message posting', expected_labels: '~"group::import"', expected_message: 'Some labels are not valid: ~"abcd"'
      end

      context 'when no labels are provided' do
        let(:labels) { '' }

        it_behaves_like 'message posting', expected_labels: '', expected_message: 'No labels specified'
      end

      context 'when command tries to set an unknown label, and API returns a "note cannot be blank" 400 error' do
        let(:labels) { '~"group::unknown"' }

        it_behaves_like 'message posting', response_body: { message: %(400 Bad request - Note {:note=>["can't be blank"]}) }, response_status: 400
      end

      context 'when command tries to set an unknown label, and API returns another 400 error' do
        let(:labels) { '~"group::unknown"' }

        it_behaves_like 'message posting', response_body: { message: %(400 Bad request - Another error) }, response_status: 400, expected_error: Gitlab::Error::BadRequest
      end
    end
  end
end
