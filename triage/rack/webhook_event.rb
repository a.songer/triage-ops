# frozen_string_literal: true

require 'rack'

require_relative '../triage/event'

module Triage
  module Rack
    WebhookEvent = Struct.new(:app) do
      def call(env)
        payload = JSON.parse("[#{::Rack::Request.new(env).body.read}]").first || {}

        app.call(env.merge(payload: payload))
      rescue JSON::ParserError => e
        ::Rack::Response.new([JSON.dump({ status: :error, error: e.class, message: e.message })], 400).finish
      end
    end
  end
end
