# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/pipeline_tier_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::PipelineTierLabel do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:merge_request_iid) { 300 }

    let(:approved) { false }
    let(:approvals_required) { 2 }
    let(:approvals_left) { 2 }
    let(:approvers) { [] }

    let(:approvals) do
      Gitlab::ObjectifiedHash.new(
        approved: approved,
        approvals_required: approvals_required,
        approvals_left: approvals_left
      )
    end

    let(:label_names) { ['bug::availability'] }

    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approved',
        gitlab_bot_event_actor?: false,
        from_gitlab_org_gitlab?: true,
        team_member_author?: true,
        automation_author?: false,
        jihu_contributor?: false,
        source_branch_is?: false,
        target_branch_is_stable_branch?: false,
        approvals: approvals,
        approvers: approvers,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  subject { described_class.new(event) }

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  include_examples 'registers listeners', [
    'merge_request.open',
    'merge_request.approval',
    'merge_request.approved',
    'merge_request.unapproval',
    'merge_request.unapproved',
    'merge_request.update'
  ]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event project is under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when event project is under gitlab-org/security/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_security_gitlab?)
          .and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when the event is coming from a bot' do
      before do
        allow(event).to receive(:gitlab_bot_event_actor?)
          .and_return(true)
      end

      include_examples 'event is not applicable'
    end

    describe 'need_pipeline_tier_label?' do
      before do
        fake_instance = instance_double(Triage::NeedMrApprovedLabel)
        allow(Triage::NeedMrApprovedLabel).to receive(:new).and_return(fake_instance)
        allow(fake_instance).to receive(:need_mr_approved_label?).and_return(need_pipeline_tier_label_value)
      end

      context 'when need_pipeline_tier_label? is true' do
        let(:need_pipeline_tier_label_value) { true }

        include_examples 'event is applicable'
      end

      context 'when need_mr_approved_label? is false' do
        let(:need_pipeline_tier_label_value) { false }

        include_examples 'event is not applicable'
      end
    end

    context 'when `pipeline::expedited` is set' do
      let(:label_names) { ['pipeline::expedited'] }

      include_examples 'event is not applicable'
    end

    context 'when `pipeline:expedite` is set' do
      let(:label_names) { ['pipeline:expedite'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:approved)           { false }
    let(:approvals_required) { 2 }
    let(:approvals_left)     { 2 }
    let(:approvers)          { [] }

    before do
      stub_api_request(
        path: "/projects/12345/issues/6789/notes",
        query: { per_page: 100 },
        response_body: []
      )
    end

    context 'when the MR was not approved by anybody' do
      let(:approvers) { [] }

      it 'adds the pipeline::tier-1 label' do
        body = <<~MARKDOWN.chomp
          /label ~\"#{Labels::PIPELINE_TIER_1_LABEL}\"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end

      context 'when the label is already present' do
        let(:label_names) { [Labels::PIPELINE_TIER_1_LABEL] }

        it 'does not add the same label again' do
          expect(subject).not_to receive(:add_comment)

          subject.process
        end
      end
    end

    context 'when the MR was approved, but still needs some approvals' do
      let(:approved)       { true }
      let(:approvals_left) { 1 }
      let(:approvers)      { [{ 'id' => 567, 'username' => 'julie' }] }

      it 'adds the pipeline::tier-2 label' do
        body = <<~MARKDOWN.chomp
          /label ~\"#{Labels::PIPELINE_TIER_2_LABEL}\"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end

      context 'when the label is already present' do
        let(:label_names) { [Labels::PIPELINE_TIER_2_LABEL] }

        it 'does not add the same label again' do
          expect(subject).not_to receive(:add_comment)

          subject.process
        end
      end
    end

    context 'when the MR has all the approvals it needs' do
      let(:approved)       { true }
      let(:approvals_left) { 0 }
      let(:approvers) do
        [
          { 'id' => 567, 'username' => 'julie' },
          { 'id' => 123, 'username' => 'bob' }
        ]
      end

      it 'adds the pipeline::tier-3 label' do
        body = <<~MARKDOWN.chomp
          /label ~\"#{Labels::PIPELINE_TIER_3_LABEL}\"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end

      context 'when the label is already present' do
        let(:label_names) { [Labels::PIPELINE_TIER_3_LABEL] }

        it 'does not add the same label again' do
          expect(subject).not_to receive(:add_comment)

          subject.process
        end
      end
    end

    context 'when the MR is in an unknown state' do
      # Note: This state below should never happen. It's just for testing purposes
      let(:approved)       { false }
      let(:approvals_left) { 0 }
      let(:approvers)      { [{}] }

      it 'does not add any labels' do
        expect(subject).not_to receive(:add_comment)

        subject.process
      end
    end
  end
end
