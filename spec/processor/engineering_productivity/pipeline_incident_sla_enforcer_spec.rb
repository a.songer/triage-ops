# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/processor/engineering_productivity/pipeline_incident_sla_enforcer'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::PipelineIncidentSlaEnforcer do
  include_context 'with event', Triage::IncidentEvent do
    let(:is_master_broken_incident_project) { true }
    let(:gitlab_bot_event_actor) { true }
    let(:event_attrs) do
      { from_master_broken_incidents_project?: is_master_broken_incident_project,
        gitlab_bot_event_actor?: gitlab_bot_event_actor,
        label_names: label_names }
    end

    let(:label_names) { ['master:broken'] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['incident.open']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event is not from master-broken-incident project' do
      let(:is_master_broken_incident_project) { false }

      include_examples 'event is not applicable'
    end

    context 'when event actor is not gitlab-bot' do
      let(:gitlab_bot_event_actor) { false }

      include_examples 'event is not applicable'
    end

    context 'when event does not have master:broken label' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'schedules GroupReminderJob, GroupWarningJob, StageWarningJob and a DevEscalationJob' do
      expect(Triage::PipelineIncidentSla::GroupReminderJob).to receive(:perform_in).with(630, event) # 10 min + 30 seconds
      expect(Triage::PipelineIncidentSla::GroupWarningJob).to receive(:perform_in).with(3630, event) # 1 hour + 30 seconds
      expect(Triage::PipelineIncidentSla::StageWarningJob).to receive(:perform_in).with(5430, event) # 1.5 hours + 30 seconds
      expect(Triage::PipelineIncidentSla::DevEscalationJob).to receive(:perform_in).with(7230, event) # 2 hours + 30 seconds

      subject.process
    end
  end
end
