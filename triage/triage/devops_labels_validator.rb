# frozen_string_literal: true
require_relative '../triage'
require_relative '../../lib/constants/labels'
require_relative '../../lib/hierarchy/group'

module Triage
  class DevopsLabelsValidator
    attr_reader :label_names

    def initialize(label_names)
      @label_names = label_names
    end

    def labels_set?
      special_team_labels? || has_group_label? || has_category_label?
    end

    private

    def special_team_labels?
      (label_names & Labels::SPECIAL_ISSUE_LABELS).flatten.any?
    end

    def has_group_label?
      (Hierarchy::Group.all_labels & label_names).any?
    end

    def has_category_label?
      label_names.any? { |label_name| label_name.start_with?('Category:') }
    end
  end
end
