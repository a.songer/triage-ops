.common_limits: &common_limits
  limits:
    most_recent: 100

.common_summary_rule_actions: &common_summary_rule_actions
  item: |
    - [ ] #{full_resource_reference}+ (by `{{author}}`) - {{labels}}

resource_rules:
  merge_requests:
    rules:
      - name: Untriaged community merge requests requiring initial triage
        conditions:
          state: opened
          labels:
            - Community contribution
          ruby: untriaged? && [14292404, 11511606].exclude?(resource[:project_id])
        <<: *common_limits
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            <<: *common_summary_rule_actions
            title: |
              #{Date.today.iso8601} Untriaged community merge requests requiring initial triage
            summary: |
              Hi merge request coaches,

              Here is a list of the ~"Community contribution" merge requests that do not have a type, stage, nor group label. We would like to ask you to:

              1. If missing, add a [type label](https://docs.gitlab.com/ee/development/labels/#type-labels).
              1. If missing, add a [group label](https://docs.gitlab.com/ee/development/labels/#group-labels).\\
                 NOTE: Some group labels are not sufficient (e.g. ~"group::distribution" or ~"group::gitaly" and you will need to use the lower level labels such as ~"group::gitaly::cluster")
              1. If missing, add a [stage label](https://docs.gitlab.com/ee/development/labels/#stage-labels).
              1. Add relevant [category](https://docs.gitlab.com/ee/development/labels/#category-labels) labels to facilitate automatic addition of stage and group labels.
              1. If no appropriate stage and group label, add a [department or team label](https://docs.gitlab.com/ee/development/labels/#department-labels).
              1. If an MR appears to be fixing a security vulnerability that you believe should not be public, please ask the AppSec team in the sec-appsec Slack channel.

              Many times, using the quick action `/copy_metadata <issue link>` can fill in the necessary information. Please double-check to ensure the metadata is correct.

              For the merge requests triaged please check off the box in front of the given merge request.

              If the merge request is ready for review, assign a reviewer based on the following criteria:

              If a group label is applied, assign a relevant reviewer from the [product group](https://about.gitlab.com/handbook/product/categories/). If none of the relevant reviewers have capacity, ping the engineering manager for the group.

              If it's not clear who the relevant reviewers for this merge request are, assign a reviewer [using reviewer roulette](https://gitlab-org.gitlab.io/gitlab-roulette/); the reviewer does not need to be an MR coach.

              Once you've triaged all the merge requests assigned to you, you can unassign and unsubscribe yourself via these quick actions:

              ```
              /done
              /unassign me
              /unsubscribe
              ```

              **When all the checkboxes are done, close the issue, and celebrate!** :tada:

              #{ coaches = merge_request_coaches; nil }
              #{ distribute_and_display_items_per_triager(resource[:items].lines(chomp: true), coaches) }

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/untriaged-community-merge-requests.yml)

              /label ~"triage report" ~"Community contribution"
