# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/pajamas_missing_workflow_label_or_weight'
require_relative '../../triage/triage/event'
require_relative '../../triage/triage'

RSpec.describe Triage::PajamasMissingWorkflowLabelOrWeight do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        iid: 1,
        project_id: project_id,
        with_project_id?: with_project_id?,
        action: 'open',
        label_names: label_names,
        weight: weight
      }
    end

    let(:label_names) { %w[foundations UX] }
    let(:project_id) { described_class::PAJAMAS_PROJECT_ID }
    let(:with_project_id?) { true }
    let(:weight) { 3 }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
  end

  include_examples 'registers listeners', ["issue.open", "issue.update"]

  include_examples 'registers listeners', ["issue.open", "issue.update"]

  describe '#applicable?' do
    context 'when does not have a workflow label added' do
      include_examples 'event is applicable'
    end

    context 'when does not have weight set' do
      let(:weight) { nil }

      include_examples 'event is applicable'
    end

    context 'when is not in Pajamas project' do
      before do
        expect(event).to receive(:with_project_id?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts missing label or weight message' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          Hi @#{event.event_actor_username},

          Please make sure that both a [workflow label](https://about.gitlab.com/handbook/product-development-flow/) and [weight](https://about.gitlab.com/handbook/engineering/ux/product-designer/#ux-issue-weights) are added to your issue. This helps to categorize and locate issues in the future.

          Thanks for your help! :heart:
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
