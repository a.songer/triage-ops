# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/workflow/devops_labels_nudger'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::Workflow::DevopsLabelsNudger do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        by_team_member?: true,
        from_gitlab_org_gitlab?: true
      }
    end
  end

  subject { described_class.new(event) }

  describe '#applicable?' do
    it_behaves_like 'applicable on contextual event'

    context 'when event labels include group label' do
      let(:label_names) { ['group::group1'] }

      include_examples 'event is not applicable'
    end

    context 'when event labels include a Category label' do
      let(:label_names) { ['Category:API'] }

      include_examples 'event is not applicable'
    end

    context 'when event labels include special team label' do
      let(:label_names) { ['meta'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'schedules a DevopsLabelsNudgerJob 5 minutes later' do
      expect(Triage::DevopsLabelsNudgerJob).to receive(:perform_in).with(Triage::DEFAULT_ASYNC_DELAY_MINUTES, event)
      subject.process
    end
  end
end
